<!doctype>
<html>
<head>
	<title>AndrewNyland.net Design 8/16/15</title>
<?php $homepage = true;
include($_SERVER["DOCUMENT_ROOT"] . "/header.php"); ?>
	<div class="page-wrap">
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<a href="photo"><img src="/1.jpg"/></a>
				</div>
			</div>
		</div>
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<h2>A title</h2>
					<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
					<a href="article" class="view-article">View Article</a>
				</div>
			</div>
		</div>
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<a href="photo"><img src="/2.jpg"/></a>
				</div>
			</div>
		</div>
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<a href="photo"><img src="/3.jpg"/></a>
				</div>
			</div>
		</div>
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<a href="photo"><img src="/4.jpg"/></a>
				</div>
			</div>
		</div>
		<div class="post-grid">
			<div class="post-wrap">
				<div class="post-inner">
					<h2>A title</h2>
					<p>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis.</p>
					<a href="photo" class="view-article">View Article</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . "/footer.php"); ?>
</body>
</html>