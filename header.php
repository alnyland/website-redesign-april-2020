<link rel="stylesheet" href="/styles/style.css" type="text/css">
<meta name=viewport content="width=device-width, initial-scale=1">
<script src="/scripts/script.js"></script>
</head>
<body class="<?php if ($homepage) {echo 'home-page';} else if ($photopage) {echo 'photo-page';}?>">
<header>
	<a href="/" class="me-description" style="background: url('/images/logo1l.png') no-repeat left; background-size: auto 100%;">
		<span class="me-inner" id="me1">Designer.</span>
		 <span class="me-inner" id="me2">Programmer.</span>
		  <span class="me-inner" id="me3">Writer.</span>
		   <span class="me-inner" id="me4">Thinker.</span>
	</a>
	<nav>
		<a href="/">Home</a>
		<a href="">Portfolio</a>
		<a href="">Contact</a>
		<a href="">About</a>
		<a href="">Blog</a>
	</nav>
	<div class="clearfix"></div>
</header>