<!doctype>
<html>
<head>
	<title>Andrew Nyland | Single Article</title>
<?php include($_SERVER["DOCUMENT_ROOT"] . "/header.php"); ?>
	<div class="article-wrap">
		<article>
			<?php include("article.html");?>
		</article>
	</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . "/footer.php"); ?>
</body>
</html>